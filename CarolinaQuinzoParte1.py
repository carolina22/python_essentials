#PARTE UNA
#CABECERA DEL EJERCICIO
print("Empezando a trabajar con Python")
print("Realizado por: Carolina Quinzo")
print("")
print("Consultando los tipos de valores")
print("")
print("")
#PRESENTANDO EL TIPO DE DATO
print("El tipo de dato de 875 es:")
print(type(875))
print("")
#PRESENTANDO EL TIPO DE DATO
print("El tipo de dato de 4.89 es:")
print(type(4.89))
print("")
#PRESENTANDO EL TIPO DE DATO DE UNA CADENA DE CARACTERES
print("El tipo de dato del texto:Now is better than never. es:")
print(type("Now is better than never."))
print("")
print("El tipo de dato de 1.32 es:")
print(type(1.32))
print("")
#COMPARANDO DATOS CON TIPOS DE DATOS
print("¿El valor 5 + 8i corresponde a un valor entero?")
print(isinstance("5+8i" , int))
print("")
#COMPARANDO DATOS CON TIPOS DE DATOS
print("¿El valor 8.2 corresponde a un valor entero?")
print(isinstance(8.2 , int))
print("")
#COMPARANDO DATOS CON TIPOS DE DATOS
print("¿El texto:Readability counts. corresponde a un string?")
print(isinstance("Readability counts.", str))







